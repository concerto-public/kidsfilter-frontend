import { createApp } from 'vue'
import App from './App.vue';

// import PrimeVue from 'primevue/config';
// import InputText from 'primevue/inputtext';
// import Button from 'primevue/button';
// import Toast from 'primevue/toast';
// import ToastService from 'primevue/toastservice';
//
// import 'primevue/resources/themes/saga-blue/theme.css';
// import 'primevue/resources/primevue.min.css';
// import 'primeicons/primeicons.css';
// import 'primeflex/primeflex.css';
import ElementPlus from 'element-plus'
import '@/css/base.css';
import 'element-plus/lib/theme-chalk/index.css';

const app = createApp(App);

app.use(ElementPlus);
// app.use(PrimeVue);
// app.use(ToastService);

// app.component('InputText', InputText);
// app.component('Button', Button);
// app.component('Toast', Toast);

app.mount('#app')
