import BaseApi from "./api.js";

export default class KidsApi extends BaseApi {
  constructor() {
    super( `http://localhost:5001`,`kids`);
  }

  async getThumbnails(namedets) {
    let urls = await this.getWithoutCache(`/thumbnails`, { namedets: namedets });
    urls = Object.fromEntries(Object.entries(urls).map(([namedet, url]) => [namedet, this.baseUrl + `/` + url]))
    return urls
  }

  async getMetadata() {
    return await this.getWithoutCache(`/metadata`);
  }

  async export(kids_list) {
    return await this.post(`/export`, kids_list);
  }

  async getFullImage(namedet) {
    let url = await this.getWithoutCache(`/full`, { namedet: namedet });
    url = this.baseUrl + `/` + url
    return url
  }
}
