export default class Kid{
    constructor(metadata) {
        this.namedet = metadata.namedet
        this.amplitude = metadata.amplitude
        this.x0 = metadata.x0
        this.y0 = metadata.y0
        this.fwhm_x = metadata.fwhm_x
        this.fwhm_y = metadata.fwhm_y
        this.theta = metadata.theta
        this.offset = metadata.offset
        this.bad = metadata.QA ?? false; // TODO make QA a bit field, will do for now
        this.src = ""
        this.matrix = metadata.matrix
    }

    toggle_bad_flag() {
        this.bad = !this.bad
    }
}