/**
 * Base classes for mvc
 */

export default class BaseApi {
  constructor(baseurl, route) {
    this.baseUrl = baseurl
    this.route = route
    this.baseApi = baseurl + "/api/" + route;
    this.getOptions = {
      method: "GET",
      cache: "force-cache",
      headers: {
        "Content-Type": "application/json",
        accepts: "application/json",
      },
    };

    this.putOptions = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        accepts: "application/json",
      },
    };

    this.getOptionsNoCache = {
      method: "GET",
      cache: "no-cache",
      headers: {
        "Content-Type": "application/json",
        accepts: "application/json",
      },
    };

    this.postOptions = {
      method: "POST",
      cache: "force-cache",
      headers: {
        "Content-Type": "application/json",
        accepts: "application/json",
      },
    };
    this.items = new Map();
  }

  async get(url, queryParameters = {}) {
    let options = this.getOptions;
    let queryString =
      "?" +
      Object.keys(queryParameters)
        .map((key) => key + "=" + queryParameters[key])
        .join("&");
    let _url = this.baseApi + url + queryString;
    //       console.log("getting " + _url + " at " + Date.now())
    return await fetch(_url, options).then(BaseApi.handleResponse);
  }

  async put(url, queryParameters = {}) {
    let options = this.putOptions;
    let queryString =
      "?" +
      Object.keys(queryParameters)
        .map((key) => key + "=" + queryParameters[key])
        .join("&");
    let _url = this.baseApi + url + queryString;
    //       console.log("getting " + _url + " at " + Date.now())
    return await fetch(_url, options).then(BaseApi.handleResponse);
  }

  async getWithoutCache(url, queryParameters = {}) {
    let options = this.getOptionsNoCache;
    let queryString =
      "?" +
      Object.keys(queryParameters)
        .map((key) => key + "=" + queryParameters[key])
        .join("&");
    let _url = this.baseApi + url + queryString;
    //       console.log("getting " + _url + " at " + Date.now())
    return await fetch(_url, options).then(BaseApi.handleResponse);
  }

  async getAndStoreJson(url, item, queryParameters = {}) {
    let options = this.getOptions;
    let queryString =
      "?" +
      Object.keys(queryParameters)
        .map((key) => key + "=" + queryParameters[key])
        .join("&");
    let _url = this.baseApi + url + queryString;
    return await fetch(_url, options).then((response) =>
      this.handleAndStoreResponse(response, item)
    );
  }

  async getWithCache(url, item) {
    if (this.items.has(item)) return this.items[item];
    else await this.getAndStoreJson(url, item);
    return this.items[item];
  }

  // async getOne(objId) {
  //     let url = `/api/${this.endpoint}/${objId}`;
  //     return await fetch(url, this.getOptions)
  //         .then(BaseApi.handleResponse);
  // }

  async post(url, payload, queryParameters = {}) {
    let options = this.postOptions;
    options.body = JSON.stringify(payload);
    let queryString =
      "?" +
      Object.keys(queryParameters)
        .map((key) => key + "=" + queryParameters[key])
        .join("&");
    let _url = this.baseApi + url + queryString;
    return await fetch(_url, options).then(BaseApi.handleResponse);
  }

  async postAndStoreJson(url, payload) {
    let options = this.postOptions;
    options.body = JSON.stringify(payload);
    return await fetch(this.baseApi + url, options).then(
      BaseApi.handlAndStoreResponse
    );
  }

  // async create(obj) {
  //     let options = this.postOptions;
  //     options.body = JSON.stringify(obj);
  //     return await fetch(`/api/${this.endpoint}`, options)
  //         .then(BaseApi.handleResponse);
  // }

  static handleResponse(response) {
    //     console.log("handling response " + response + " at " + Date.now())
    let contentType = response.headers.get("content-type");
    if (contentType.includes("application/json")) {
      return BaseApi.handleJSONResponse(response);
    } else if (contentType.includes("text/html")) {
      return BaseApi.handleTextResponse(response);
    } else {
      return Promise.reject(response.status);
    }
  }

  handleAndStoreResponse(response, key) {
    let contentType = response.headers.get("content-type");
    if (contentType.includes("application/json")) {
      return this.handleAndStoreJSONResponse(response, key);
    } else if (contentType.includes("text/html")) {
      return this.handleTextResponse(response);
    } else {
      return Promise.reject(response.status);
    }
  }

  static handleJSONResponse(response) {
    return response.json().then((json) => {
      if (response.ok) {
        return json;
      } else {
        return Promise.reject(
          Object.assign({}, json, {
            status: response.status,
            statusText: response.statusText,
          })
        );
      }
    });
  }

  handleAndStoreJSONResponse(response, key) {
    var myself = this;
    return response.json().then((json) => {
      if (response.ok) {
        myself.items[key] = json;
        return json;
      } else {
        return Promise.reject(
          Object.assign({}, json, {
            status: response.status,
            statusText: response.statusText,
          })
        );
      }
    });
  }

  static handleTextResponse(response) {
    return response.text().then((text) => {
      if (response.ok) {
        return text;
      } else {
        return Promise.reject({
          status: response.status,
          statusText: response.statusText,
          err: text,
        });
      }
    });
  }
}
